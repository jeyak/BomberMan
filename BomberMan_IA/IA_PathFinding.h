#include "IA_A_Star_Node.h"
#include "IA_Node.h"
#include <vector>

#pragma once
class IA_PathFinding
{
public:
	IA_PathFinding(IA_Node player, IA_Node target);
	~IA_PathFinding();

	std::vector<std::vector<IA_A_Star_Node> > getMap() const;
	char getMvt() const;

	void initMapping(std::vector<std::vector<char> > _mapDispatcher);
	int distance(int x, int y);
	void updateNeighbour(IA_Node node);
	void calcNeighbour(int x, int y, int cost);
	IA_Node chooseNode();
	IA_Node pathFindingBack(IA_Node n);

	friend std::ostream& operator<<(std::ostream& os, const IA_PathFinding& pathFinding);

private:
	std::vector<std::vector<char> > _mapDispatcher;
	std::vector<std::vector<IA_A_Star_Node> > _map;

	int _hSize;
	int _wSize;

	IA_Node _player;
	IA_Node _target;

	char mvt;	
};

