#include "stdafx.h"


Player::Player() {
	this->_id = 0;
    this->_bombLeft = 0;
    this->_range = 0;
    this->_duration = 0;
	this->_turn = 1;
	this->_deadline = 0;
	this->_infinite = true;
}

Player::~Player() {
    std::cerr << "destroy" << std::endl;
}

// GETTER & SETTER

int Player::getTurn() const {
	return this->_turn;
}

int Player::getId() const {
	return this->_id;
}

int Player::getBombLeft() const {
	return this->_bombLeft;
}

int Player::getRange() const {
	return this->_range;
}

int Player::getDelay() const {
	return this->_duration;
}


int Player::getDeadline() const {
	return this->_deadline;
}

bool Player::getInfinite() const {
	return this->_infinite;
}

// INITIALIZE PLAYER METHOD

void Player::startPlayer() {
	std::string value;
	std::getline(std::cin, value);
	Tools::guidLineInput("START player", value);

	// get id player
	std::string id;
	std::getline(std::cin, id);
	this->_id = atoi(id.c_str());

	std::getline(std::cin, value);
	Tools::guidLineInput("STOP player", value);
}

void Player::settingsPlayer() {
	std::string value;
	std::getline(std::cin, value);
	Tools::guidLineInput("START settings", value);

	std::string attribut;
	std::string titleAttribut;
	std::string number;

	// get all attribut

	do {
		std::getline(std::cin, attribut);
		std::size_t foundIndex = attribut.find(" ");
		titleAttribut = attribut.substr(0, foundIndex);
		number = attribut.substr(foundIndex + 1, attribut.length());

		if (titleAttribut.compare("NB_BOMBS") == 0)
			this->_bombLeft = atoi(number.c_str());

		if (titleAttribut.compare("BOMB_DURATION") == 0)
			this->_duration = atoi(number.c_str());

		if (titleAttribut.compare("BOMB_RADIUS") == 0)
			this->_range = atoi(number.c_str());

		if (titleAttribut.compare("DEADLINE") == 0) {
			this->_deadline = atoi(number.c_str());
			this->_infinite = false;
		}

	} while (attribut.compare("STOP settings") != 0);

	Tools::guidLineInput("STOP settings", attribut);
}

void Player::startPlayerTurn() {
	std::string value;
	std::getline(std::cin, value);

	std::string startTurn = "START turn ";
	startTurn.append(std::to_string(this->_turn));

	Tools::guidLineInput(startTurn, value);
}

void Player::stopPlayerTurn() {
	std::string stopTurn = "STOP turn ";
	stopTurn.append(std::to_string(this->_turn));

	std::string value;
	std::getline(std::cin, value);
	Tools::guidLineInput(stopTurn, value);
}

void Player::startPlayerAction() {
	std::string startAction = "START action ";
	startAction.append(std::to_string(this->_turn));

	std::cout << startAction << std::endl;
}

void Player::stopPlayerAction() {
	std::string stopAction = "STOP action ";
	stopAction.append(std::to_string(this->_turn));

	std::cout << stopAction << std::endl;
}

void Player::nextTurn() {
	this->_turn++;
}

// ACTION

void Player::walk(char mvt) {
	std::cout << mvt << std::endl;
}

void Player::plant(IA_Node me) {
	if (this->_bombLeft == 0) {
		return;
	}
	this->_bombLeft--;
	Bomb b = Bomb(me.getX(), me.getY(), this->_duration);
	this->_myBombPlant.push_back(b);
	std::cout << "B" << std::endl;
}

void Player::delayMyBomb() {

	if (this->_myBombPlant.size() == 0) {
		return;
	}

	for (int i = 0; i < this->_myBombPlant.size(); i++) {
		this->_myBombPlant[i].updateDelay();
	}

	if (this->_myBombPlant[0].getDelay() == 0) {
		this->_myBombPlant.erase(this->_myBombPlant.begin());
		this->_bombLeft++;
	}
}

IA_Node Player::escape(IA_Node me, std::vector<std::vector<char> > map, int id) {

	char playerBomb = id + 51;

	// CHECK VERTICALLY DOWN
	for (int i = me.getX(); i <= me.getX() + this->_range; i++) {
		if (i >= 0 && i < map.size()) {
			if (map[i][me.getY()] == '#' || map[i][me.getY()] == '%') {
				break;
			}

			if (map[i][me.getY()] == 'o' || map[i][me.getY()] == playerBomb) {
				return IA_Node(i, me.getY());
			}
		}
	}

	// CHECK VERTICALLY UP
	for (int i = me.getX(); i >= me.getX() - this->_range; i--) {
		if (i >= 0 && i < map.size()) {
			if (map[i][me.getY()] == '#' || map[i][me.getY()] == '%') {
				break;
			}

			if (map[i][me.getY()] == 'o' || map[i][me.getY()] == playerBomb) {
				return IA_Node(i, me.getY());
			}
		}
	}

	// CHECK HORIZONTALLY LEFT
	for (int i = me.getY(); i <= me.getY() + this->_range; i++) {
		if (i >= 0 && i < map.size()) {
			if (map[me.getX()][i] == '#' || map[me.getX()][i] == '%') {
				break;
			}

			if (map[me.getX()][i] == 'o' || map[me.getX()][i] == playerBomb) {
				return IA_Node(me.getX(), i);
			}
		}
	}

	// CHECK HORIZONTALLY RIGHT
	for (int i = me.getY(); i >= me.getY() - this->_range; i--) {
		if (i >= 0 && i < map.size()) {
			if (map[me.getX()][i] == '#' || map[me.getX()][i] == '%') {
				break;
			}

			if (map[me.getX()][i] == 'o' || map[me.getX()][i] == playerBomb) {
				return IA_Node(me.getX(), i);
			}
		}
	}

	return IA_Node();
}


std::ostream& operator<<(std::ostream& os, const Player& player) {
	os << "Player : id = " << player.getId() << ", trun = " << player.getTurn() << ", bomb_left = " << player.getBombLeft() << ", bomb_range = " << player.getRange() << ", bomb_delay = " << player.getDelay() << std::endl;
	return os;
}

