#pragma once
class IA_Node
{
public:
	IA_Node();
	IA_Node(int x, int y);
	~IA_Node();

	int getX() const;
	int getY() const;

	friend std::ostream& operator<<(std::ostream& os, const IA_Node& a_star_node);

private:
	int x;
	int y;
};

