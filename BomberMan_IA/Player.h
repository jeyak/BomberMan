#ifndef PLAYER_H
#define PLAYER_H
#include <iostream>
#include <string>
#include <vector>
#include "Bomb.h"
#include "IA_Node.h"
#include "Map.h"

class Player {
    public:
        Player();
        virtual ~Player();

		// GETTER & SETTER
		int getTurn() const;
		int getId() const;
		int getBombLeft() const;
		int getRange() const;
		int getDelay() const;
		int getDeadline() const;
		bool getInfinite() const;

		// INITIALIZE PLAYER METHOD
		void startPlayer();
		void settingsPlayer();
		void startPlayerTurn();
		void stopPlayerTurn();
		void startPlayerAction();
		void stopPlayerAction();
		void nextTurn();

		// ACTION
		void walk(char mvt);
		void plant(IA_Node me);
		void delayMyBomb();
		IA_Node escape(IA_Node me, std::vector<std::vector<char> > map, int id);

		friend std::ostream& operator<<(std::ostream& os, const Player& player);

    private:
		int _id;
        int _bombLeft;
        int _range;
        int _duration;
		int _turn;
		int _deadline;
		bool _infinite;
		std::vector<Bomb> _myBombPlant;
};

#endif // PLAYER_H
