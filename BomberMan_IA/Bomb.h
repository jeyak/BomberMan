#pragma once
class Bomb
{
public:
	Bomb(int x, int y, int delay);
	~Bomb();

	int getDelay() const;
	int getX() const;
	int getY() const;

	void updateDelay();

	friend std::ostream& operator<<(std::ostream& os, const Bomb& bomb);

private:
	int _x;
	int _y; 
	int _delay;
};

