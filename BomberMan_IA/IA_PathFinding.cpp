#include "stdafx.h"
#include "IA_PathFinding.h"


IA_PathFinding::IA_PathFinding(IA_Node player, IA_Node target) {
	this->_player = player;
	this->_target = target;
}


IA_PathFinding::~IA_PathFinding() {
}


std::vector<std::vector<IA_A_Star_Node> > IA_PathFinding::getMap() const {
	return this->_map;
}

void IA_PathFinding::initMapping(std::vector<std::vector<char> > mapDispatcher) {
	this->_hSize = mapDispatcher.size();
	this->_wSize = mapDispatcher[0].size();

	std::vector<std::vector<IA_A_Star_Node> > map(this->_hSize, std::vector<IA_A_Star_Node>(this->_wSize));

	for (int i = 0; i < mapDispatcher.size(); i++) {
		for (int j = 0; j < mapDispatcher[i].size(); j++) {
			if (mapDispatcher[i][j] == '#' || mapDispatcher[i][j] == 'o' || mapDispatcher[i][j] == '%') {
				map[i][j] = IA_A_Star_Node(-5);
			}
			else if (i == this->_player.getX() && j == this->_player.getY()) {
				map[i][j] = IA_A_Star_Node(-10);
			}
			else {
				map[i][j] = IA_A_Star_Node(-1);
			}
		}
	}
	this->_map = map;
}


void IA_PathFinding::updateNeighbour(IA_Node node) {
	int x = node.getX();
	int y = node.getY();

	int cost = this->_map[x][y].getCost();
	
	if (x == 0 || x == this->_hSize - 1 || y == 0 || y == this->_wSize - 1)
		return;

	// UP NEIGHBOUR
	if (this->_map[x - 1][y].getTotal() != -5 && this->_map[x - 1][y].getTotal() != -10 && !this->_map[x - 1][y].getExplored()) {
		calcNeighbour(x - 1, y, cost);
	}

	// DOWS NEIGHBOUR
	if (this->_map[x + 1][y].getTotal() != -5 && this->_map[x + 1][y].getTotal() != -10 && !this->_map[x + 1][y].getExplored()) {
		calcNeighbour(x + 1, y, cost);
	}

	// LEFT NEIGHBOUR
	if (this->_map[x][y - 1].getTotal() != -5 && this->_map[x][y - 1].getTotal() != -10 && !this->_map[x][y - 1].getExplored()) {
		calcNeighbour(x, y - 1, cost);
	}

	// RIGHT NEIGHBOUR
	if (this->_map[x][y + 1].getTotal() != -5 && this->_map[x][y + 1].getTotal() != -10 && !this->_map[x][y + 1].getExplored()) {
		calcNeighbour(x, y + 1, cost);
	}

	this->_map[x][y].setEplored();
}


void IA_PathFinding::calcNeighbour(int x, int y, int cost) {
	int countDestination = distance(x, y) * 10;

	if (this->_map[x][y].getTotal() == -1) {
		this->_map[x][y] = IA_A_Star_Node(countDestination, cost + 10);
		return;
	}

	if ((countDestination + cost) <= this->_map[x][y].getTotal() && countDestination < this->_map[x][y].getDistance()) {
		this->_map[x][y] = IA_A_Star_Node(countDestination, cost);
	}
}

IA_Node IA_PathFinding::chooseNode() {
	int lowTotal = 9999;
	int lowDistance = 9999;
	int minX = -1;
	int minY = -1;

	for (int i = 0; i < this->_map.size(); i++) {
		for (int j = 0; j < this->_map[i].size(); j++) {
			if (this->_map[i][j].getTotal() > 0 && this->_map[i][j].getTotal() < lowTotal && this->_map[i][j].getDistance() < lowDistance && this->_map[i][j].getExplored() == false) {
				lowTotal = this->_map[i][j].getTotal();
				lowDistance = this->_map[i][j].getDistance();
				minX = i;
				minY = j;
			}
		}
	}
	return IA_Node(minX, minY);
}

IA_Node IA_PathFinding::pathFindingBack(IA_Node n) {
	int total = this->_map[n.getX()][n.getY()].getTotal();
	bool explored = this->_map[n.getX()][n.getY()].getExplored();
	int cost = this->_map[n.getX()][n.getY()].getCost();

	IA_Node nodeToReturn = IA_Node();
	
	if ((this->_map[n.getX() - 1][n.getY()].getTotal() <= total && this->_map[n.getX() - 1][n.getY()].getExplored()) || this->_map[n.getX() - 1][n.getY()].getTotal() == -10) {
		
		if (this->_map[n.getX() - 1][n.getY()].getCost() < cost) {
			nodeToReturn = IA_Node(n.getX() - 1, n.getY());
			this->mvt = 'D';
		}
	}

	if ((this->_map[n.getX() + 1][n.getY()].getTotal() <= total && this->_map[n.getX() + 1][n.getY()].getExplored()) || this->_map[n.getX() + 1][n.getY()].getTotal() == -10) {

		if (this->_map[n.getX() + 1][n.getY()].getCost() < cost) {
			nodeToReturn = IA_Node(n.getX() + 1, n.getY());
			this->mvt = 'U';
		}
	}

	if ((this->_map[n.getX()][n.getY() - 1].getTotal() <= total && this->_map[n.getX()][n.getY() - 1].getExplored()) || this->_map[n.getX()][n.getY() - 1].getTotal() == -10) {
		
		if (this->_map[n.getX()][n.getY() - 1].getCost() < cost) {
			nodeToReturn = IA_Node(n.getX(), n.getY() - 1);
			this->mvt = 'R';
		}
	}

	if ((this->_map[n.getX()][n.getY() + 1].getTotal() <= total && this->_map[n.getX()][n.getY() + 1].getExplored()) || this->_map[n.getX()][n.getY() + 1].getTotal() == -10) {	

		if (this->_map[n.getX()][n.getY() + 1].getCost() < cost) {
			nodeToReturn = IA_Node(n.getX(), n.getY() + 1);
			this->mvt = 'L';
		}
	}


	this->_map[nodeToReturn.getX()][nodeToReturn.getY()].setUneplored();
	return nodeToReturn;
}


std::ostream& operator<<(std::ostream& os, const IA_PathFinding& pathFinding) {
	for (int i = 0; i < pathFinding.getMap().size(); i++) {
		for (int j = 0; j < pathFinding.getMap()[i].size(); j++) {
			os << pathFinding.getMap()[i][j].getTotal() << " |\t";
		}
		os << std::endl;
	}
	os << std::endl;

	return os;
}


int IA_PathFinding::distance(int x, int y) {
	return std::abs(x - this->_target.getX()) + std::abs(y - this->_target.getY());
}


char IA_PathFinding::getMvt() const {
	return this->mvt;
}
