// stdafx.h : fichier Include pour les fichiers Include système standard,
// ou les fichiers Include spécifiques aux projets qui sont utilisés fréquemment,
// et sont rarement modifiés
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include "Player.h"
#include "Map.h"
#include "Tools.h"
#include "IA_Node.h"
#include "IA_PathFinding.h"


// TODO: faites référence ici aux en-têtes supplémentaires nécessaires au programme
