#include "stdafx.h"
#include "Bomb.h"


Bomb::Bomb(int x, int y, int delay)
{
	this->_x = x;
	this->_y = y;
	this->_delay = delay;
}


Bomb::~Bomb()
{
}

int Bomb::getDelay() const {
	return this->_delay;
}

int Bomb::getX() const {
	return this->_x;
}

int Bomb::getY() const {
	return this->_y;
}

void Bomb::updateDelay() {
	this->_delay--;
}

std::ostream& operator<<(std::ostream& os, const Bomb& bomb) {
	os << "bomb [x, y, delay] : [" << bomb.getX() << ", " << bomb.getY() << ", " << bomb.getDelay() << "]" << std::endl;
	return os;
}