#include "stdafx.h"
#include "Tools.h"


Tools::Tools()
{
}


Tools::~Tools()
{
}

// TOOLS

void Tools::guidLineInput(std::string data, std::string value) {
	if (data.compare(value) != 0) {
		std::cout << "Expected input '" << data << "' but found '" << value << "'" << std::endl;
		exit(EXIT_FAILURE);
	}
}
