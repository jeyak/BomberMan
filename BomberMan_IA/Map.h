#include <vector>
#include "IA_Node.h"
#include "stdafx.h"

#pragma once
class Map
{
public:
	Map();
	~Map();

	// GETTER & SETTER
	std::vector<std::vector<char> > getMap() const;
	int getHSize() const;
	int getWSize() const;

	void getSizeFromDispatcher(std::string size);
	void getMapFromDispatcher();

	IA_Node getPositionPlayer(int playerNumber);

	friend std::ostream& operator<<(std::ostream& os, const Map& map);

private:
	int _hSize;
	int _wSize;
	std::vector<std::vector<char> > _map;
};

