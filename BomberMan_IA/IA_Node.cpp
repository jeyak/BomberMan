#include "stdafx.h"
#include "IA_Node.h"

IA_Node::IA_Node() {
	this->x = -1;
	this->y = -1;
}

IA_Node::IA_Node(int x, int y) {
	this->x = x;
	this->y = y;
}

int IA_Node::getX() const {
	return this->x;
}

int IA_Node::getY() const {
	return this->y;
}

IA_Node::~IA_Node()
{
}

std::ostream& operator<<(std::ostream& os, const IA_Node& node) {
	os << "node [x, y] : [" << node.getX() << ", " << node.getY() << "]" << std::endl;
	return os;
}