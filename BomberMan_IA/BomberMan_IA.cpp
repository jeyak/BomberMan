// BomberMan_IA.cpp : définit le point d'entrée pour l'application console.
//

#include "stdafx.h"
#include <map>


int main()
{
	Player p = Player();
	Map m = Map();
	
	int cmp = 0;

	p.startPlayer();
	p.settingsPlayer();

	while (p.getInfinite() || p.getDeadline() > 0) {
		p.startPlayerTurn();
		std::string text;
		std::getline(std::cin, text);
		if (text.size() > 5 && text.substr(0, 5).compare("WINNER")) {
			break;
		}

		m.getSizeFromDispatcher(text);
		m.getMapFromDispatcher();

		p.stopPlayerTurn();

		IA_Node me = m.getPositionPlayer(p.getId());
		if (me.getX() == -1 && me.getY() == -1) {
			return 0;
		}

		p.delayMyBomb();

		// GET ALL PLAYER
		std::multimap<int, IA_Node> dict;

		for (int i = 1; i < 5; i++) {
			if (i != p.getId()) { // ALL EXECPT ME
				IA_Node otherPlayer = m.getPositionPlayer(i);
				if (otherPlayer.getX() != -1 && otherPlayer.getY() != -1) {
					int distance = std::abs(me.getX() - otherPlayer.getX()) + std::abs(me.getY() - otherPlayer.getY());
					dict.insert(std::pair<int, IA_Node>(distance, otherPlayer));
				}
			}
		}

		// FIND THE NEAREEST PLAYER

		int nearestPlayer = 9999;

		for (std::multimap<int, IA_Node>::iterator it = dict.begin(); it != dict.end(); ++it) {
			if (it->first < nearestPlayer)
				nearestPlayer = it->first;
		}

		if (dict.size() > 0) {
			IA_Node target = dict.find(nearestPlayer)->second;

			IA_Node bombNear = p.escape(me, m.getMap(), p.getId());

			bool escapeBool = false;

			if (bombNear.getX() != -1 && bombNear.getY() != -1) {
				if (bombNear.getY() == me.getY() && m.getMap()[me.getX()][me.getY() - 1] != '#' && m.getMap()[me.getX()][me.getY() - 1] != 'o' && m.getMap()[me.getX()][me.getY() - 1] != '%') {
					target = IA_Node(me.getX(), me.getY() - 1);
				}
				else if (bombNear.getY() == me.getY() && m.getMap()[me.getX()][me.getY() + 1] != '#' && m.getMap()[me.getX()][me.getY() + 1] != 'o' && m.getMap()[me.getX()][me.getY() - 1] != '%') {
					target = IA_Node(me.getX(), me.getY() + 1);
				}
				else if (bombNear.getX() == me.getX() && m.getMap()[me.getX() - 1][me.getY()] != '#' && m.getMap()[me.getX() - 1][me.getY()] != 'o' && m.getMap()[me.getX()][me.getY() - 1] != '%') {
					target = IA_Node(me.getX() - 1, me.getY());
				}
				else if (bombNear.getX() == me.getX() && m.getMap()[me.getX() + 1][me.getY()] != '#' && m.getMap()[me.getX() + 1][me.getY()] != 'o' && m.getMap()[me.getX()][me.getY() - 1] != '%') {
					target = IA_Node(me.getX() + 1, me.getY());
				}
				escapeBool = true;
			}

			if (target.getX() == -1 && target.getY() == -1) {
				p.startPlayerAction();
				p.stopPlayerAction();
			}
			// TEST SI ESCAPE
			else if (!escapeBool && p.getBombLeft() > 0 && std::abs(me.getX() - target.getX()) + std::abs(me.getY() - target.getY()) <= 4 && m.getMap()[me.getX()][me.getY()] != p.getId() + 51) {
				p.startPlayerAction();
				p.plant(me);
				p.stopPlayerAction();
			}
			else {

				IA_Node nextNode = IA_Node();

				IA_PathFinding pf = IA_PathFinding(me, target);
				pf.initMapping(m.getMap());

				pf.updateNeighbour(me);

				int i = 0;
				do {
					nextNode = pf.chooseNode();
					if (nextNode.getX() == target.getX() && nextNode.getY() == target.getY()) {
						break;
					}
					pf.updateNeighbour(nextNode);
					i++;
				} while (i < (m.getHSize() * m.getWSize()));

				do {
					nextNode = pf.pathFindingBack(nextNode);
					i--;
				} while (i > 0 && m.getMap()[nextNode.getX()][nextNode.getY()] != p.getId() + 48 && m.getMap()[nextNode.getX()][nextNode.getY()] != p.getId() + 52);

				char mvt = pf.getMvt();

				p.startPlayerAction();
				p.walk(mvt);
				p.stopPlayerAction();
			}
		}

		p.nextTurn();
		cmp++;
	}
	return 0;
}

