#include "stdafx.h"
#include "Map.h"


Map::Map()
{
}


Map::~Map()
{
}

// GET & SETTER
std::vector<std::vector<char> > Map::getMap() const {
	return this->_map;
}


int Map::getHSize() const {
	return this->_hSize;
}

int Map::getWSize() const {
	return this->_wSize;
}


//

void Map::getSizeFromDispatcher(std::string size) {
	std::size_t found = size.find(" ");
	this->_hSize = atoi(size.substr(0, found).c_str());
	this->_wSize = atoi(size.substr(found + 1, size.length()).c_str());
}

void Map::getMapFromDispatcher() {
	std::string line;
	std::vector<std::vector<char> > map(this->_hSize, std::vector<char>(this->_wSize));

	int h = 0;
	int w = 0; // w and i represent width
	int i = 0; // i is used to read `line`

	do {
		i = 0;
		w = 0;
		std::getline(std::cin, line);
		while (line[i] != '\0' || i < this->_wSize) {
			map[h][w] = line[i];
			i++;
			w++;
		}
		h++;
	} while (h != this->_hSize);

	this->_map = map;
}

//

IA_Node Map::getPositionPlayer(int playerNumber) {

	char np = playerNumber + 48;
	char npAndB = playerNumber + 52; // 48 = 0 in ASCII code

	for (int i = 0; i < this->_map.size(); i++) {
		for (int j = 0; j < this->_map[i].size(); j++) {
			if (this->_map[i][j] == np || this->_map[i][j] == npAndB) {
				return IA_Node(i, j);
			}
		}
	}
	return IA_Node();
}

std::ostream& operator<<(std::ostream& os, const Map& map) {
	for (int i = 0; i < map.getMap().size(); i++) {
		for (int j = 0; j < map.getMap()[i].size(); j++) {
			os << map.getMap()[i][j] << " | ";
		}
		os << std::endl;
	}
	os << std::endl;
	return os;
}