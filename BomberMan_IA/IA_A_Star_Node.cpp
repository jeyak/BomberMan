#include "stdafx.h"
#include "IA_A_Star_Node.h"


IA_A_Star_Node::IA_A_Star_Node()
{
}

IA_A_Star_Node::IA_A_Star_Node(int total)
{
	this->_explored = false;
	this->_cost = 0;
	this->_distance = 0;
	this->_total = total;
}

IA_A_Star_Node::IA_A_Star_Node(int d, int cost) {
	this->_distance = d;
	this->_cost = cost;
	this->_total = d + cost;
	this->_explored = false;
}

IA_A_Star_Node::~IA_A_Star_Node()
{
}

int IA_A_Star_Node::getDistance() const {
	return this->_distance;
}

int IA_A_Star_Node::getCost() const {
	return this->_cost;
}

int IA_A_Star_Node::getTotal() const {
	return this->_total;
}

bool IA_A_Star_Node::getExplored() const {
	return this->_explored;
}

void IA_A_Star_Node::setEplored() {
	this->_explored = true;
}

void IA_A_Star_Node::setUneplored() {
	this->_explored = false;
}

std::ostream& operator<<(std::ostream& os, const IA_A_Star_Node& IA_A_Star_Node) {
	os << "IA_A_Star_Node [total, cost, distance, explored] : [" << IA_A_Star_Node.getTotal() << ", " << IA_A_Star_Node.getCost() << ", " << IA_A_Star_Node.getDistance() << ", " << IA_A_Star_Node.getExplored() << "]" << std::endl;
	return os;
}
