#pragma once
class IA_A_Star_Node
{

public:
	IA_A_Star_Node();
	IA_A_Star_Node(int total);
	IA_A_Star_Node(int d, int cost);
	~IA_A_Star_Node();

	int getDistance() const;
	int getCost() const;
	int getTotal() const;
	bool getExplored() const;

	void setEplored();
	void setUneplored();

	friend std::ostream& operator<<(std::ostream& os, const IA_A_Star_Node& a_star_node);

private:
	int _distance;
	int _cost;
	int _total;
	bool _explored;
};

