#pragma once

//map
#define HEIGHTMAP 1920
#define WIDTHMAP 1080

//brick texture
#define BRICKLENGTH  32
#define BRICKHEIGHT  32

#define MARGE 100
#define LEN(arr) ((int) (sizeof (arr) / sizeof (arr)[0]))

class BomberManUI
{
private:
	sf::RenderWindow *window = nullptr;
	int p1Pos = 64;
	int p2Pos = 64;
	int p3Pos = 64;
	int p4Pos = 64;
	void updatePosForPlayer(int playerId, int posValue);
public:
	BomberManUI();
	~BomberManUI();
	void updateGrid(char gridValues[][HEIGHT], char(*grid)[WIDTH][HEIGHT], string action = " ", int playerId = 0);
	void displayWindow();
	bool isWindowsOpened();
};