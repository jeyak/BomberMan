#include "stdafx.h"
#include "BomberManUI.h"


void BomberManUI::updatePosForPlayer(int playerId, int posValue)
{
	switch (playerId) {
	case 1:
		this->p1Pos = posValue;
		break;
	case 2:
		this->p2Pos = posValue;
		break;
	case 3:
		this->p3Pos = posValue;
		break;
	case 4:
		this->p4Pos = posValue;
		break;
	}
}

BomberManUI::BomberManUI() {
	this->window = new sf::RenderWindow(sf::VideoMode((BRICKHEIGHT*HEIGHT) + (BRICKHEIGHT * 2), (BRICKLENGTH*WIDTH) + (BRICKLENGTH * 2)), "SFML Bomberman");
}

BomberManUI::~BomberManUI()
{
	delete this->window;
}

void BomberManUI::updateGrid(char gridValues[][HEIGHT], char(*grid)[WIDTH][HEIGHT], string action, int playerId) {

	const unsigned int total = sizeof(*grid);
	unsigned int largeur = sizeof((*grid)[0]);
	unsigned int longeur = total / largeur;

	unsigned int c = 0;
	unsigned int j = 0;

	sf::Texture player1;
	sf::Texture player2;
	sf::Texture player3;
	sf::Texture player4;
	sf::Texture brick;
	sf::Texture rock;
	sf::Texture bombe;
	sf::Texture down;
	sf::Sprite spriteElem[total];

	if (action != " " && playerId != 0) {
		if (action == "U") {
			this->updatePosForPlayer(playerId, 128);
		} 
		else if (action == "D") {
			this->updatePosForPlayer(playerId, 64);
		}
		else if (action == "L") {
			this->updatePosForPlayer(playerId, 32);
		}
		else if (action == "R") {
			this->updatePosForPlayer(playerId, 96);
		}
	}

	if (!player1.loadFromFile("H:\\rjeya\\Documents\\Sources\\Repos\\ESGI\\C++\\BomberManSrc\\BomberMan\\img\\player1.png", sf::IntRect(0, this->p1Pos, 64, 64))) {
		cout << "error chargement player1.png " << endl;
		exit(1);
	}

	if (!player2.loadFromFile("H:\\rjeya\\Documents\\Sources\\Repos\\ESGI\\C++\\BomberManSrc\\BomberMan\\img\\player2.png", sf::IntRect(0, this->p2Pos, 64, 64))) {
		cout << "error chargement player2.png " << endl;
		exit(1);
	}

	if (!player3.loadFromFile("H:\\rjeya\\Documents\\Sources\\Repos\\ESGI\\C++\\BomberManSrc\\BomberMan\\img\\player3.png", sf::IntRect(0, this->p3Pos, 64, 64))) {
		cout << "error chargement player3.png " << endl;
		exit(1);
	}

	if (!player4.loadFromFile("H:\\rjeya\\Documents\\Sources\\Repos\\ESGI\\C++\\BomberManSrc\\BomberMan\\img\\player4.png", sf::IntRect(0, this->p4Pos, 64, 64))) {
		cout << "error chargement player4.png " << endl;
		exit(1);
	}

	if (!brick.loadFromFile("H:\\rjeya\\Documents\\Sources\\Repos\\ESGI\\C++\\BomberManSrc\\BomberMan\\img\\#.png")) {
		cout << "error chargement #.png" << endl;
		exit(1);
	}

	if (!down.loadFromFile("H:\\rjeya\\Documents\\Sources\\Repos\\ESGI\\C++\\BomberManSrc\\BomberMan\\img\\_.png")) {
		cout << "error chargement _.png" << endl;
		exit(1);
	}

	if (!rock.loadFromFile("H:\\rjeya\\Documents\\Sources\\Repos\\ESGI\\C++\\BomberManSrc\\BomberMan\\img\\%.png")) {
		cout << "error chargement %.png" << endl;
		exit(1);
	}

	if (!bombe.loadFromFile("H:\\rjeya\\Documents\\Sources\\Repos\\ESGI\\C++\\BomberManSrc\\BomberMan\\img\\o.png")) {
		cout << "error chargement o.png" << endl;
		exit(1);
	}

	unsigned int cnt = 0;
	for (c = 0; c < longeur; c++) {
		for (j = 0; j < largeur; j++) {
			switch (gridValues[c][j]) {
			case '1':
				spriteElem[cnt].setTexture(player1);
				spriteElem[cnt].setPosition(BRICKHEIGHT*c + BRICKHEIGHT, BRICKLENGTH*j + BRICKLENGTH);
				break;
			case '2':
				spriteElem[cnt].setTexture(player2);
				spriteElem[cnt].setPosition(BRICKHEIGHT*c + BRICKHEIGHT, BRICKLENGTH*j + BRICKLENGTH);
				break;
			case '3':
				spriteElem[cnt].setTexture(player3);
				spriteElem[cnt].setPosition(BRICKHEIGHT*c + BRICKHEIGHT, BRICKLENGTH*j + BRICKLENGTH);
				break;
			case '4':
				spriteElem[cnt].setTexture(player4);
				spriteElem[cnt].setPosition(BRICKHEIGHT*c + BRICKHEIGHT, BRICKLENGTH*j + BRICKLENGTH);
				break;
			case '5':
				spriteElem[cnt].setTexture(player1);
				spriteElem[cnt].setPosition(BRICKHEIGHT*c + BRICKHEIGHT, BRICKLENGTH*j + BRICKLENGTH);
				break;
			case '6':
				spriteElem[cnt].setTexture(player2);
				spriteElem[cnt].setPosition(BRICKHEIGHT*c + BRICKHEIGHT, BRICKLENGTH*j + BRICKLENGTH);
				break;
			case '7':
				spriteElem[cnt].setTexture(player3);
				spriteElem[cnt].setPosition(BRICKHEIGHT*c + BRICKHEIGHT, BRICKLENGTH*j + BRICKLENGTH);
				break;
			case '8':
				spriteElem[cnt].setTexture(player4);
				spriteElem[cnt].setPosition(BRICKHEIGHT*c + BRICKHEIGHT, BRICKLENGTH*j + BRICKLENGTH);
				break;
			case '#':
				spriteElem[cnt].setTexture(brick);
				spriteElem[cnt].setPosition(BRICKHEIGHT*c + BRICKHEIGHT, BRICKLENGTH*j + BRICKLENGTH);
				break;
			case '%':
				spriteElem[cnt].setTexture(rock);
				spriteElem[cnt].setPosition(BRICKHEIGHT*c + BRICKHEIGHT, BRICKLENGTH*j + BRICKLENGTH);
				break;
			case 'o':
				spriteElem[cnt].setTexture(bombe);
				spriteElem[cnt].setPosition(BRICKHEIGHT*c + BRICKHEIGHT, BRICKLENGTH*j + BRICKLENGTH);
				break;
			default:
				spriteElem[cnt].setTexture(down);
				spriteElem[cnt].setPosition(BRICKHEIGHT*c + BRICKHEIGHT, BRICKLENGTH*j + BRICKLENGTH);
				break;
			}
			cnt++;
		}
	}

	sf::Event event;
	while (this->window->pollEvent(event)) {
		switch (event.type) {
		case sf::Event::Closed:
			this->window->close();
			break;
		}
	}

	this->window->clear(sf::Color(71, 131, 0));


	for (int i = 0; i < total; i++) {
		this->window->draw(spriteElem[i]);
	}

	this->window->display();
}

void BomberManUI::displayWindow() {

	sf::Event event;
	while (this->window->pollEvent(event)) {
		switch (event.type) {
		case sf::Event::Closed:
			this->window->close();
			break;
		}
	}

	this->window->clear(sf::Color(255, 255, 255));
	this->window->display();
}

bool BomberManUI::isWindowsOpened()
{
	return this->window->isOpen();
}