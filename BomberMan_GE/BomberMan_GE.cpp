// BomberMan_GE.cpp : définit le point d'entrée pour l'application console.
//

#include "stdafx.h"
#include "BomberManGE.h"

int main()
{
	BomberManGE *ge = new BomberManGE();

	ge->start();

	ge->printGrid();

    return 0;
}