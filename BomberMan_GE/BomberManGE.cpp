#include "stdafx.h"
#include "BomberManGE.h"

// Bomb exploding delay
static int DELAY = 5;
// Bomb explosion radius
static int RADIUS = 5;
// NB BOMBS
const int BOMBS = 10;

BomberManGE::BomberManGE()
{
	for (int x = 0; x < WIDTH; x++) {
		for (int y = 0; y < HEIGHT; y++) {
			if (x == 0 || x == HEIGHT - 1) {
				this->grid[x][y] = '#';
			}
			else if(y == 0 || y == WIDTH - 1) {
				this->grid[x][y] = '#';
			}
			else {
				
				if (this->canIPlaceWall()) {
					this->grid[x][y] = '#';
				}
				else if(this->canIPlaceDummyWall())
				{
					this->grid[x][y] = '%';
				}
				else
				{
					this->grid[x][y] = '_';
				}
			}
		}
	}
}


BomberManGE::~BomberManGE()
{
}

void BomberManGE::start()
{
	// fix true random issue
	srand((unsigned int)time(NULL));
	int nbplayers = 0;
	//read number of players
	this->nextInputMustBe("START players");
	cin >> nbplayers;
	cin >> ws;
	this-> nextInputMustBe("STOP players");
	cout << "START settings" << endl;
	cout << "NB_BOMBS " << BOMBS << endl;
	cout << "BOMB_DURATION " << DELAY << endl;
	cout << "BOMB_RADIUS " << RADIUS << endl;
	cout << "STOP settings" << endl;

	for (int i = 1; i <= nbplayers; i++) {
		tuple<int, int> pos = this->findRandomFreeCase();
		int x = get<0>(pos);
		int y = get<1>(pos);
		this->grid[x][y] = to_string(i).c_str()[0];
		this->player_position.insert_or_assign(i, make_tuple(x, y));
	}

	int turn = 1;

	this->bombermanUi.displayWindow();

	while (true) {
		int winner = updateBombs();
		if (!this->bombermanUi.isWindowsOpened()) {
			break;
		}

		for (int i = 1; i <= nbplayers; i++) {
			cout << "START turn " << turn << " " << i << endl;
			if (winner != NULL) {
				cout << "WINNER" << winner << endl;
			}
			else {
				cout << WIDTH << " " << HEIGHT << endl;
				this->printGrid();
			}
			cout << "STOP turn " << turn << " " << i << endl;
			this->nextInputMustBe("START actions " + to_string(turn) + " " + to_string(i));
			string action = "";
			getline(cin, action);
			this->playerAction(i, action);
			this->nextInputMustBe("STOP actions " + to_string(turn) + " " + to_string(i));
		}
		if (winner != NULL) {
			break;
		}
		turn += 1;
	}
}

void BomberManGE::printGrid()
{
	for (int x = 0; x < WIDTH; x++) {
		for (int y = 0; y < HEIGHT; y++) {
			cout << this->grid[y][x];
		}
		cout << endl;
	}

	this->bombermanUi.updateGrid(this->grid, &this->grid);
}

bool BomberManGE::canIPlaceWall()
{
	// Random assert
	int randNum = this->getRandomInt(0, 10);
	return randNum == 1;
}

bool BomberManGE::canIPlaceDummyWall()
{
	// Random assert
	int randNum = this->getRandomInt(0, 10);
	return randNum == 2;
}

void BomberManGE::nextInputMustBe(string value)
{
	string val = "";

	getline(cin, val);

	if (val != value) {
		cerr << "expected input was '" << value << "' instead of '" << val << "'" << endl;
		exit(EXIT_FAILURE);
	}
	return;
}

int BomberManGE::getRandomInt(int min, int max)
{
	int randNum = rand() % (max - min + 1) + min;
	return randNum;
}

tuple<int, int> BomberManGE::findRandomFreeCase()
{
	int x = 0;
	int y = 0;
	while (true) {
		x = this->getRandomInt(1, WIDTH);
		y = this->getRandomInt(1, HEIGHT);

		if (this->grid[x][y] == '_') {
			break;
		}
	}

	return make_tuple(x, y);
}

void BomberManGE::playerAction(int playerId, string action)
{
	if (action == "NOACTION") {
		return;
	}

	if (this->player_position.find(playerId) == this->player_position.end()) {
		return;
	}

	tuple<int, int> playerPosition = player_position.at(playerId);

	int bombMask = 0;
	int x = get<0>(playerPosition);
	int y = get<1>(playerPosition);

	char currentPositionValue = this->grid[x][y];

	if (currentPositionValue == '5' || currentPositionValue == '6' || currentPositionValue == '7' || currentPositionValue == '8') {
		this->grid[x][y] = 'o';
		bombMask = 4;
	}
	else {
		grid[x][y] = '_';
	}

	if (action == "U" && grid[x][y - 1] == '_') {
		bombMask = 0;
		y -= 1;
	} else if (action == "D" && grid[x][y + 1] == '_') {
		bombMask = 0;
		y += 1;
	} else if (action == "L" && grid[x - 1][y] == '_') {
		bombMask = 0;
		x -= 1;
	} else if (action == "R" && grid[x + 1][y] == '_') {
		bombMask = 0;
		x += 1;
	} else if (action == "B" &&  (grid[x][y] == '_' || grid[x][y] == (to_string(playerId).c_str())[0])) {
		bombMask = 4;
		bombs.insert_or_assign(playerPosition, DELAY);
	}

	grid[x][y] = (to_string(playerId + bombMask).c_str())[0];

	this->player_position.insert_or_assign(playerId, make_tuple(x, y));
	this->bombermanUi.updateGrid(this->grid, &this->grid, action, playerId);
}

void BomberManGE::triggerBomb(int x, int y)
{
	this->bombs.erase(make_tuple(x, y));
	this->grid[x][y] = '_';


	tuple<int, int> directions[] = { {0,1}, {0, -1 }, {1,0}, {-1, 0} };

	for(auto direction : directions)
	{
		int dx = get<0>(direction);
		int dy = get<1>(direction);

		for (int i = 1; i <= RADIUS; i++) {
			int cx = x + i * dx;
			int cy = y + i * dy;

			char currentPositionValue = this->grid[cx][cy];

			if (currentPositionValue == '5' || currentPositionValue == '6' || currentPositionValue == '7' || currentPositionValue == '8') {
				int playerId = getPlayerIdFromChar(this->grid[cx][cy]);
				this->grid[cx][cy] = '_';
				cerr << "bomb{" << x << "," << y << "}: del" << playerId << "dans{";
				for (auto position : player_position) {
					cerr << get<0>(position.second) << ",";
					cerr << get<1>(position.second) << "";
				}
				cerr << "}";
				player_position.erase(playerId);
				triggerBomb(cx, cy);
			}
			else if (currentPositionValue == '1' || currentPositionValue == '2' || currentPositionValue == '3' || currentPositionValue == '4') {
				int playerId = getPlayerIdFromChar(this->grid[cx][cy]);
				this->grid[cx][cy] = '_';
				cerr << "bomb{" << x << "," << y << "}: del" << playerId << "dans{";
				for (auto position : player_position) {
					cerr << get<0>(position.second) << ",";
					cerr << get<1>(position.second) << "";
				}
				cerr << "}";
				player_position.erase(playerId);
			}
			else if (currentPositionValue == 'o') {
				this->grid[cx][cy] = '_';
				triggerBomb(cx, cy);
			}
			else if (currentPositionValue == '%') {
				this->grid[cx][cy] = '_';
				break;
			}
		}
	}
}

int BomberManGE::getPlayerIdFromChar(char c) {
	switch (c)
	{
		case '1':
			return 1;
			break;
		case '2':
			return 2;
			break;
		case '3':
			return 3;
			break;
		case '4':
			return 4;
			break;
		case '5':
			return 1;
			break;
		case '6':
			return 2;
			break;
		case '7':
			return 3;
			break;
		case '8':
			return 4;
			break;
		default:
			break;
	}
	return -1;
}

int BomberManGE::updateBombs()
{
	map<tuple<int, int>, int> bombsCopy(bombs);
	if (bombsCopy.size() > 0) {
		map<tuple<int, int>, int>::iterator bombPos;
		for (bombPos = bombsCopy.begin(); bombPos != bombsCopy.end(); bombPos++) {
			if (bombPos != bombsCopy.end()) {
				auto key = bombPos->first;
				auto val = bombPos->second;
				if (val == 1) {
					triggerBomb(get<0>(key), get<1>(key));
					if (bombsCopy.size() == 0 || bombPos == bombsCopy.end()) {
						break;
					}
				}
				else {
					bombs.insert_or_assign(key, val - 1);
				}
				if (player_position.size() == 1) {
					return player_position.begin()->first;
				}
				if (player_position.size() == 0) {
					return -1;
				}
			}
		}
	}

	return 0;
}
