#pragma once

// Grid default size

class BomberManGE
{
	private:
		BomberManUI bombermanUi;
		// Grid
		char grid[WIDTH][HEIGHT];
		map<int, tuple<int, int>> player_position;
		map<tuple<int, int>, int> bombs;
		bool canIPlaceWall();
		bool canIPlaceDummyWall();
		void nextInputMustBe(string value);
		int getRandomInt(int min = 0, int max = 0);
		tuple<int, int> findRandomFreeCase();
		void playerAction(int playerId, string action);
		void triggerBomb(int x, int y);
		int updateBombs();
		int getPlayerIdFromChar(char c);

	public:
		BomberManGE();
		~BomberManGE();
		void start();
		void printGrid();
};

