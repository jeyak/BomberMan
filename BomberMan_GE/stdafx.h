// stdafx.h : fichier Include pour les fichiers Include système standard,
// ou les fichiers Include spécifiques aux projets qui sont utilisés fréquemment,
// et sont rarement modifiés
//

#pragma once

using namespace std;

//GE
#define WIDTH 20
#define HEIGHT 20

#include "targetver.h"
#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <tuple>
#include <string>
#include <sstream>
#include <list>
#include <map>
#include <cmath>
#include <ctime>
#include <SFML/Graphics.hpp>
#include "BomberManUI.h"


#ifdef _MSC_VER
#define DEBUG_BREAK __debugbreak()
#else
#endif


// TODO: faites référence ici aux en-têtes supplémentaires nécessaires au programme
