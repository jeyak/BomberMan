# Bomberman C++ - 16/07/2018

**Retrouver le sujet [ici](./Sujet.pdf "sujet")**

## Répartion des tâches

@Jeyak (Jeyaksan)

- développement du Game Engine
- débogage GE + IA
- integration UI dans la GE (en partie)
- débogage sur differente partie
- implémentation du bonus mur déstructible dans le game engine

@Ahmedthedev (Ahmed)
- développement de l'ui en sfml
- implémentation du bonus mur déstructible dans l'ui
- integration UI dans la GE (en partie)
- débogage sur differente partie

@ndennu (Nicolas)

- développement de l'Intelligence Artificielle
- débogage GE + IA
- implémentation du bonus mur déstructible dans l'IA

## Architecture du programme

Le jeu est découpé en 3 parties distinctent, le Game Engine (dont interface graphique basé sur les sprites), l'Intélligence Artificelle et le dispatcher (fournis).

### Architecture du Game Engine

Le game engine n'utilise qu'une classe pour fonctionner et utilise une deuxième classe pour afficher et gérer l'interface graphique (l'interface graphique utilise la librairie SFML)

 - BomberManGE
 - BomberManUI

### Architecture intelligence artificielle

Le programme de L'intelligence artificielle contient 8 classes qui permettent de gérer un grande partie des possibilités.

- Bomb
- IA_A_Star_Node
- IA_Node
- IA_PathFinding
- Map
- Player
- Tools
- BomberMan_IA (main)

Certaines de ces classes sont des classes utilitaires qui permettent de manipuler des données plus facilement comme IA_Node ou IA_A_Star_Node.

#### BomberMan_IA (main)

Ci-dessous la cinématique de la classe.

![architecture IA](https://gitlab.com/jeyak/BomberMan/raw/master/img/schemaIA.PNG)

Au moment de son tour, chaque joueur a 4 possibilités de jeu.

La boucle 'while(1)' permet d’enchaîner les tours en alternance avec les autres joueurs.

- Se déplacer
- Poser une bombe
- Eviter une bombe
- Ne rien faire

#### Se déplacer

Le déplacement d'un joueur est basé sur l'algorithme A* avec l'heuristique de Manhattan.
Cette algorithme permet de trouvé le chemin le plus court pour aller sur la case souhaité. Seul les déplacements HAUT, BAS, DROITE, GAUCHE sont autorisés, pas mouvement en diagonale.

La cible peut être un autre joueur ou bien un une case vide si le joueur tente d'éviter une bombe. En cas de sélection d'un autre joueur le joueur choisira comme cible celui le plus proche (à vol d'oiseaux).
Cette algorithme est exécuté à chaque tour car les adversaire peuvent également se déplacer ou une bombe peut être placé.

#### Poser une bombe

Si le joueur a une distance strictement inférieur à 5 case (à vol d'oiseaux), ce dernier va automatiquement poser une bombe.

*Note: Pour poser une bombe le joueur ne doit pas être en train d'esquiver une bombe*.

#### Eviter une bombe

Si le joueur se trouve dans le rayon d'explosion d'une bombe, ce dernier va tenter de l'éviter. En fonction de son positionnement avec la bombe son déplacement sera différent. S'il se trouve sur l'axe des abscisses (horizontal), il effectuera un déplacement vers le HAUT ou le BAS (si les cases ne sont pas des murs). Inversement s'il se trouve sur l'axe des ordonnées (vertical), il effectuera une déplacement vers la GAUCHE ou la DROITE.

![escape](https://gitlab.com/jeyak/BomberMan/raw/master/img/escape.png)

#### Ne rien faire

Si le joueur ne peut pas atteindre un autre joueur ou ne pose pas de bombe il passe sont tour et ne fait rien.

## Bonus Implémentés

| Bonus                                                 | GE    | IA    |
| ----------------------------------------------------- |:-----:|:-----:|
| Interface graphique (autre que ligne de commande)     | Oui   | -     |
| Item - modifier le rayons de destruction des bombes   | Non   | Non   |
| Item - modifier le delais d'explosion des bombes      | Non   | Non   |
| Item - modifier le de bombes maximum                  | Non   | Non   |
| Item - déclencher manuellement les bombes             | Non   | Non   |
| Item - modifier le nombre de coups par tour           | Non   | Non   |
| Plateau de jeu torique                                | Non   | -     |
| Murs destructibles                                    | Oui   | Oui   |
| Nombre de tour limite                                 | Non   | Oui   |
| Enregistrer les coups dans un fichier                 | Non   | Non   |
